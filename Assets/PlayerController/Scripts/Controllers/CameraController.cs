using Cinemachine;
using System;
using System.Collections;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [field: SerializeField] public bool UsingOrbitCamera { get; private set; }
    [field: SerializeField] public GameObject playerEyes { get; private set; }

    [field: SerializeField] public HumanoidLandInput input { get; private set; }
    [field: SerializeField] public float cameraZoomModifier { get; private set; } = 32f;
    
    float minCameraZoomDistance = 2f;
    float minOrbitCameraZoomDistance = 1f;
    float maxCameraZoomDistance = 12f;
    float maxOrbitCameraZoomDistance = 36f;

    CinemachineVirtualCamera activeCamera { get; set; }
    int activeCameraPriorityModifier { get; set; } = 31337;

    [field: SerializeField] public Camera MainCamera { get; private set; }
    private CinemachineBrain cinemachineBrain { get; set; }
    [field: SerializeField] public CinemachineVirtualCamera cinemachine1stPerson { get; set; }
    [field: SerializeField] public CinemachineVirtualCamera cinemachine3ndPerson { get; set; }
    CinemachineFramingTransposer cinemachineFarmingTransposer3rdPerson { get; set; }
    [field: SerializeField] public CinemachineVirtualCamera cinemachineOrvit { get; set; }
    CinemachineFramingTransposer cinemachineFarmingTransposerOrbit { get; set; }

    Vector3 startPos { get; set; }
    Quaternion startRot { get; set; }

    private void Awake() {
        cinemachineBrain = MainCamera.GetComponent<CinemachineBrain>();
        cinemachineFarmingTransposer3rdPerson = cinemachine3ndPerson.GetCinemachineComponent<CinemachineFramingTransposer>();
        cinemachineFarmingTransposerOrbit = cinemachineOrvit.GetCinemachineComponent<CinemachineFramingTransposer>();
    }

    // Start is called before the first frame update
    void Start() {
        //ChangeCamera(); // First time through, lets set the camera.
        SetCameraPriorities(cinemachine3ndPerson, cinemachine1stPerson);
        playerEyes.SetActive(false);
        
        startPos = activeCamera.gameObject.transform.position;
        startRot = activeCamera.gameObject.transform.rotation;
    }

    // Update is called once per frame
    void Update() {
        if (!(input.zoomCameraInput == 0f))
            ZoomCamera();
        if (input.ChangeCameraWasPressedThisFrame)
            ChangeCamera();
        /*if (input.CenterCameraWasPressedThisFrame)
            CenterCamera();*/
    }

    private void ZoomCamera() {
        if (activeCamera == cinemachine3ndPerson) {
            cinemachineFarmingTransposer3rdPerson.m_CameraDistance = 
                Mathf.Clamp(cinemachineFarmingTransposer3rdPerson.m_CameraDistance + 
                (input.InvertScroll ? -input.zoomCameraInput : input.zoomCameraInput) / cameraZoomModifier,
                minCameraZoomDistance, maxCameraZoomDistance);
        }
        else if (activeCamera == cinemachineOrvit) {
            cinemachineFarmingTransposerOrbit.m_CameraDistance =
                Mathf.Clamp(cinemachineFarmingTransposerOrbit.m_CameraDistance +
                (input.InvertScroll ? -input.zoomCameraInput : input.zoomCameraInput) / cameraZoomModifier,
                minOrbitCameraZoomDistance, maxOrbitCameraZoomDistance);
        }
    }

    private void CenterCamera() {
        Debug.Log($"Centering Camera {startPos}");
        activeCamera.PreviousStateIsValid = false;
        activeCamera.gameObject.transform.position = Vector3.zero;
        activeCamera.PreviousStateIsValid = true;
    }

    void ChangeCamera() {
        if (cinemachine3ndPerson == activeCamera) {
            SetCameraPriorities(cinemachine3ndPerson, cinemachine1stPerson);
            UsingOrbitCamera = false;
            //MainCamera.cullingMask &= ~(1 << LayerMask.NameToLayer("Player(Self)"));
            StartCoroutine(DisablePlayerParts(cinemachineBrain.m_DefaultBlend.BlendTime));
        }
        else if (cinemachine1stPerson == activeCamera) {
            SetCameraPriorities(cinemachine1stPerson, cinemachineOrvit);
            UsingOrbitCamera = true;
            //MainCamera.cullingMask |= 1 << LayerMask.NameToLayer("Player(Self)");
        }
        else if (cinemachineOrvit == activeCamera) {
            SetCameraPriorities(cinemachineOrvit, cinemachine3ndPerson);
            activeCamera = cinemachine3ndPerson;
            UsingOrbitCamera = false;
        }
        else {  // for the first time through or if there's an error.
            cinemachine3ndPerson.Priority += activeCameraPriorityModifier;
            activeCamera = cinemachine3ndPerson;
        }
    }

    private void SetCameraPriorities(CinemachineVirtualCamera currentCameraMode, CinemachineVirtualCamera newCameraMode) {
        currentCameraMode.Priority -= activeCameraPriorityModifier;
        newCameraMode.Priority += activeCameraPriorityModifier;
        activeCamera = newCameraMode;
        
        if (newCameraMode != cinemachine1stPerson)
            playerEyes.SetActive(true);
    }

    IEnumerator DisablePlayerParts(float blendTime) {
        yield return new WaitForSeconds(blendTime);
        playerEyes.SetActive(false);
    }
}
