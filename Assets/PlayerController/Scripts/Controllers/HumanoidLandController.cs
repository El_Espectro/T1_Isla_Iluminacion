
using System;
using UnityEngine;

public class HumanoidLandController : MonoBehaviour {

    [field: SerializeField] public Transform CameraFollow { get; private set; }

#pragma warning disable CS0108 // El miembro oculta el miembro heredado. Falta una contraseņa nueva
    [field: SerializeField] public Rigidbody rigidbody { get; private set; }
    [field: SerializeField] public CapsuleCollider capsuleCollider { get; private set; }
#pragma warning restore CS0108 // El miembro oculta el miembro heredado. Falta una contraseņa nueva
    [field: SerializeField] public HumanoidLandInput input { get; private set; }
    [field: SerializeField] public CameraController cameraController { get; private set; }

    Vector3 playerMoveInput = Vector3.zero;

    Vector3 playerLookInput = Vector3.zero;
    Vector3 previousPlayerLookInput = Vector3.zero;
    float cameraPitch { get; set; } = 0f;
    [field: SerializeField] public float playerLookInputLerpTime { get; private set; } = 0.35f;

    [field: Header("Movement")]
    [field: SerializeField] public float movementMultiplier { get; private set; } = 30f;
    [field: SerializeField] public float notGroundedMovementMultiplier { get; private set; } = 1.25f;
    [field: SerializeField] public float runMultiplier { get; private set; } = 2.5f;
    [field: SerializeField] public float rotationSpeedMultiplier { get; private set; } = 180f;
    [field: SerializeField] public float pitchSpeedMultiplier { get; private set; } = 180f;

    [field: Header("GroundCheck")]
    [field: SerializeField] public bool playerIsGrounded { get; private set; }
    [field: SerializeField, Range(0f, 1.8f)] public float groundCheckRadiousMultiplier { get; private set; } = 0.9f;
    [field: SerializeField, Range(-0.95f, 1.05f)] public float groundCheckDistance { get; private set; } = 0.05f;
    RaycastHit groundCheckHit = new RaycastHit();

    [field: Header("Gravity")]
    [field: SerializeField] public float gravityFallCurrent { get; private set; } = -100f;
    [field: SerializeField] public float gravityFallMin { get; private set; } = -100f;
    [field: SerializeField] public float gravityFallMax { get; private set; } = -500f;
    [field: SerializeField, Range(-5f, -35f)] public float gravityFallIncrementAmount { get; private set; } = -20f;
    [field: SerializeField] public float gravityFallIncrementTime { get; private set; } = 0.05f;
    [field: SerializeField] public float playerFallTimer { get; private set; } = 0f;
    [field: SerializeField] public float gravityGrounded { get; private set; } = -1f;
    [field: SerializeField] public float maxSlopeAngle { get; private set; } = 77.5f;

    [field: Header("Jumping")]
    [field: SerializeField] public float initialJumpForce { get; private set; } = 750f;
    [field: SerializeField] public float continualJumpForceMultiplier { get; private set; } = 0.1f;
    [field: SerializeField] public float jumpTime { get; private set; } = 0.175f;
    [field: SerializeField] public float jumpTimeCounter { get; private set; } = 0f;
    [field: SerializeField] public float coyoteTime { get; private set; } = 0.15f;
    [field: SerializeField] public float coyoteTimeCounter { get; private set; } = 0f;
    [field: SerializeField] public float jumpBufferTime { get; private set; } = 0.2f;
    [field: SerializeField] public float jumpBufferTimeCounter { get; private set; } = 0f;
    [field: SerializeField] public bool playerIsJumping { get; private set; }
    [field: SerializeField] public bool jumpWasPressedLastFrame { get; private set; }


    private void Awake() {
        if (rigidbody == null)
            rigidbody = GetComponent<Rigidbody>();
        if (capsuleCollider == null)
            capsuleCollider = GetComponent<CapsuleCollider>();
    }

    private void FixedUpdate() {
        if (!cameraController.UsingOrbitCamera) {
            playerLookInput = GetLookInput();
            PlayerLook();
            PitchCamera();
        }

        playerMoveInput = GetMoveInput();
        playerIsGrounded = PlayerGroundCheck();

        playerMoveInput = PlayerMove();
        playerMoveInput = PlayerSlope();
        playerMoveInput = PlayerRun();
        
        playerMoveInput.y = PlayerFallGravity();
        playerMoveInput.y = PlayerJump();

        playerMoveInput *= rigidbody.mass;  // Note: For devs purpouse.

        rigidbody.AddRelativeForce(playerMoveInput, ForceMode.Force);
    }

    private Vector3 GetLookInput() {
        previousPlayerLookInput = playerLookInput;
        playerMoveInput = new Vector3(input.LookInput.x, (input.InvertMouseY ? -input.LookInput.y : input.LookInput.y), 0f);
        return Vector3.Lerp(previousPlayerLookInput, playerMoveInput * Time.fixedDeltaTime, playerLookInputLerpTime);
    }

    private void PlayerLook() {
        rigidbody.rotation = Quaternion.Euler(0f, rigidbody.rotation.eulerAngles.y + 
            (playerLookInput.x * rotationSpeedMultiplier), 0f);
    }

    private void PitchCamera() {
        cameraPitch += playerLookInput.y * pitchSpeedMultiplier;
        cameraPitch = Mathf.Clamp(cameraPitch, -89.9f, 89.9f);

        CameraFollow.rotation = Quaternion.Euler(cameraPitch, CameraFollow.rotation.eulerAngles.y, 
            CameraFollow.rotation.eulerAngles.z);
    }

    private Vector3 GetMoveInput() {
        return new Vector3(input.MoveInput.x, 0f, input.MoveInput.y);
    }

    private Vector3 PlayerMove() {
        /*return new Vector3(playerMoveInput.x * movementMultiplier,
                           playerMoveInput.y,
                           playerMoveInput.z * movementMultiplier);*/
        return ((playerIsGrounded) ? (playerMoveInput * movementMultiplier) : 
            (playerMoveInput * movementMultiplier * notGroundedMovementMultiplier));
    }

    private Vector3 PlayerSlope() {
        Vector3 calculatedPlayerMovement = playerMoveInput;

        if (playerIsGrounded) {
            Vector3 localGroundCheckHitNormal = rigidbody.transform.InverseTransformDirection(groundCheckHit.normal);

            float groundSlopeAngle = Vector3.Angle(localGroundCheckHitNormal, rigidbody.transform.up);

            if (groundSlopeAngle == 0f) {
                if (input.MoveIsPressed) {
                    RaycastHit rayHit;
                    float rayHeightFromGround = 0.1f;
                    float rayCalculatedRayHeight = rigidbody.transform.position.y - 
                        capsuleCollider.bounds.extents.y + rayHeightFromGround;
                    Vector3 rayOrigin = new Vector3(rigidbody.position.x, rayCalculatedRayHeight,
                        rigidbody.position.z);

                    if (Physics.Raycast(rayOrigin, rigidbody.transform.TransformDirection(calculatedPlayerMovement), 
                        out rayHit, 0.75f)) {

                        if (Vector3.Angle(rayHit.normal, rigidbody.transform.up) > maxSlopeAngle) {
                            calculatedPlayerMovement.y = -movementMultiplier;
                        }
                    }
                    Debug.DrawRay(rayOrigin, rigidbody.transform.TransformDirection(calculatedPlayerMovement),
                        Color.green, 1f);
                }

                if (calculatedPlayerMovement.y == 0f) {
                    calculatedPlayerMovement.y = gravityGrounded;
                }
            }
            else {
                Quaternion slopeAngleRotation = Quaternion.FromToRotation(rigidbody.transform.up, localGroundCheckHitNormal);
                calculatedPlayerMovement = slopeAngleRotation * calculatedPlayerMovement;

                // Para limitar la velocidad en pendientes.
                /*float relativeSlopeAngle = Vector3.Angle(calculatedPlayerMovement, rigidbody.transform.up) - 90f;
                calculatedPlayerMovement += calculatedPlayerMovement * (relativeSlopeAngle / 90f);*/

                if (groundSlopeAngle < maxSlopeAngle) {
                    if (input.MoveIsPressed) {
                        calculatedPlayerMovement.y += gravityGrounded;
                    }
                }
                /*else {
                    float calculatedSlopeGravity = groundSlopeAngle * -0.2f;
                    if (calculatedSlopeGravity < calculatedPlayerMovement.y) {
                        calculatedPlayerMovement.y = calculatedSlopeGravity;
                    }
                }*/
            }
#if UNITY_EDITOR
            Debug.DrawRay(rigidbody.position, rigidbody.transform.TransformDirection(calculatedPlayerMovement),
                Color.red, 0.5f);
#endif
        }

        return calculatedPlayerMovement;
    }

    private Vector3 PlayerRun() {
        Vector3 calculatedPlayerRunSpeed = playerMoveInput;
        if (input.MoveIsPressed && input.RunIsPressed) {
            calculatedPlayerRunSpeed *= runMultiplier;
        }

        return calculatedPlayerRunSpeed;
    }
    
    private bool PlayerGroundCheck() {
        float sphereCastRadius = capsuleCollider.radius * groundCheckRadiousMultiplier;
        float sphereCastTravelDistance = capsuleCollider.bounds.extents.y - sphereCastRadius + groundCheckDistance;

        return Physics.SphereCast(rigidbody.position, sphereCastRadius, Vector3.down, 
            out groundCheckHit, sphereCastTravelDistance);
    }

    private float PlayerFallGravity() {
        float gravity = playerMoveInput.y;

        if (playerIsGrounded) {
            gravityFallCurrent = gravityFallMin;    // Reset gravityFallCurrent to gravityFallMin
        }
        else {
            playerFallTimer -= Time.fixedDeltaTime;
            if (playerFallTimer <= 0f) {
                if (gravityFallCurrent > gravityFallMax) {
                    gravityFallCurrent += gravityFallIncrementAmount;
                }
                playerFallTimer = gravityFallIncrementTime;
            }
            gravity = gravityFallCurrent;
        }

        return gravity;
    }

    private float PlayerJump() {
        float calculatedJumpInput = playerMoveInput.y;

        SetJumpTimeCounter();
        SetCoyoteTimeCounter();
        SetJumpBufferTimeCounter();

        if (jumpBufferTimeCounter > 0f && !playerIsJumping && coyoteTimeCounter > 0f) {
            calculatedJumpInput = initialJumpForce;
            playerIsJumping = true;
            jumpBufferTimeCounter = 0f;
            coyoteTimeCounter = 0f;
        }
        else if (input.JumpIsPressed && playerIsJumping && !playerIsGrounded && jumpTimeCounter > 0f) {
            calculatedJumpInput = initialJumpForce * continualJumpForceMultiplier;
        }
        else if (playerIsJumping && playerIsGrounded) {
            playerIsJumping = false;
        }

        return calculatedJumpInput;
    }

    private void SetJumpTimeCounter() {
        if (playerIsJumping && !playerIsGrounded) {
            jumpTimeCounter -= Time.fixedDeltaTime;
        }
        else {
            jumpTimeCounter = jumpTime;
        }
    }

    private void SetCoyoteTimeCounter() {
        if (playerIsGrounded) {
            coyoteTimeCounter = coyoteTime;
        }
        else {
            coyoteTimeCounter -= Time.fixedDeltaTime;
        }
    }

    private void SetJumpBufferTimeCounter() {
        if (!jumpWasPressedLastFrame && input.JumpIsPressed) {
            jumpBufferTimeCounter = jumpBufferTime;
        }
        else if (jumpBufferTimeCounter > 0f) {
            jumpBufferTimeCounter -= Time.fixedDeltaTime;
        }
        jumpWasPressedLastFrame = input.JumpIsPressed;
    }
}
