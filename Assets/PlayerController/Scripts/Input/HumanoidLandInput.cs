using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class HumanoidLandInput : MonoBehaviour {

    public Vector2 MoveInput { get; private set; } = Vector2.zero;
    [field: SerializeField] public bool MoveIsPressed { get; private set; } = false;
    public Vector2 LookInput { get; private set; } = Vector2.zero;
    [field: SerializeField] public bool InvertMouseY { get; private set; } = true;
    [field: SerializeField] public float zoomCameraInput { get; private set; } = 0f;
    [field: SerializeField] public bool InvertScroll { get; private set; } = false;
    [field: SerializeField] public bool RunIsPressed { get; private set; } = false;
    [field: SerializeField] public bool JumpIsPressed { get; private set; } = false;

    /*[field: SerializeField] */public bool ChangeCameraWasPressedThisFrame { get; private set; } = false;
    public bool CenterCameraWasPressedThisFrame { get; private set; } = false;
    public bool PauseWasPressedThisFrame { get; private set; } = false;

    InputActions _input = null;

    private void OnEnable() {
        _input = new InputActions();
        _input.HumanoidLand.Enable();
        _input.UI.Enable();

        _input.HumanoidLand.Move.performed += SetMove;
        _input.HumanoidLand.Move.canceled += SetMove;

        _input.HumanoidLand.Look.performed += SetLook;
        _input.HumanoidLand.Look.canceled += SetLook;

        _input.HumanoidLand.Run.started += SetRun;
        _input.HumanoidLand.Run.canceled += SetRun;
        
        _input.HumanoidLand.Jump.started += SetJump;
        _input.HumanoidLand.Jump.canceled += SetJump;
        
        _input.HumanoidLand.ZoomCamera.started += SetZoomCamera;
        _input.HumanoidLand.ZoomCamera.canceled += SetZoomCamera;
    }

    private void Update() {
        ChangeCameraWasPressedThisFrame = _input.HumanoidLand.ChangeCamera.WasPressedThisFrame();
        CenterCameraWasPressedThisFrame = _input.HumanoidLand.CenterCamera.WasPressedThisFrame();
        PauseWasPressedThisFrame = _input.UI.Pause.WasPressedThisFrame();
    }

    private void SetMove(InputAction.CallbackContext ctx) {
        MoveInput = ctx.ReadValue<Vector2>();
        MoveIsPressed = !(MoveInput == Vector2.zero);
    }
    private void SetLook(InputAction.CallbackContext ctx) {
        LookInput = ctx.ReadValue<Vector2>();
    }
    private void SetZoomCamera(InputAction.CallbackContext ctx) {
        zoomCameraInput = ctx.ReadValue<float>();
    }

    private void SetRun(InputAction.CallbackContext ctx) {
        RunIsPressed = ctx.started;
    }
    
    private void SetJump(InputAction.CallbackContext ctx) {
        JumpIsPressed = ctx.started;
    }
    
    private void OnDisable() {
        _input.HumanoidLand.Move.performed -= SetMove;
        _input.HumanoidLand.Move.canceled -= SetMove;

        _input.HumanoidLand.Look.performed -= SetLook;
        _input.HumanoidLand.Look.canceled -= SetLook;

        _input.HumanoidLand.Run.started -= SetRun;
        _input.HumanoidLand.Run.canceled -= SetRun;

        _input.HumanoidLand.Jump.started -= SetJump;
        _input.HumanoidLand.Jump.canceled -= SetJump;
        
        _input.HumanoidLand.ZoomCamera.started -= SetZoomCamera;
        _input.HumanoidLand.ZoomCamera.canceled -= SetZoomCamera;

        _input.HumanoidLand.Disable();
        _input.UI.Disable();
    }
}
