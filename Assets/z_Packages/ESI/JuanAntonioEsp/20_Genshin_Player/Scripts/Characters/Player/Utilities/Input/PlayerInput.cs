using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

namespace GenshinImpactMovementSystem
{
    public class PlayerInput : MonoBehaviour {

        public PlayerInputActions InputActions { get; private set; }
        public PlayerInputActions.PlayerActions PlayerActions { get; private set; }
        
        /*[field: SerializeField] */public bool ChangeCameraWasPressedThisFrame { get; private set; } = false;
        public Vector2 LookInput { get; private set; } = Vector2.zero;

        private void Awake() {
            InputActions = new PlayerInputActions();
            PlayerActions = InputActions.Player;
        }

        private void OnEnable() {
            InputActions.Enable();

            PlayerActions.Look.performed += SetLook;
            PlayerActions.Look.canceled += SetLook;
        }
        private void Update() {
            ChangeCameraWasPressedThisFrame = PlayerActions.ChangeCamera.WasPressedThisFrame();
        }

        private void OnDisable() {
            PlayerActions.Look.performed -= SetLook;
            PlayerActions.Look.canceled -= SetLook;

            InputActions.Disable();
        }
        
        private void SetLook(InputAction.CallbackContext ctx) {
            LookInput = ctx.ReadValue<Vector2>();
        }

        public void DisableActionFor(InputAction action, float seconds) {
            StartCoroutine(DisableAction(action, seconds));
        }

        private IEnumerator DisableAction(InputAction action, float seconds) {
            action.Disable();
            yield return new WaitForSeconds(seconds);
            action.Enable();
        }
    }
}
