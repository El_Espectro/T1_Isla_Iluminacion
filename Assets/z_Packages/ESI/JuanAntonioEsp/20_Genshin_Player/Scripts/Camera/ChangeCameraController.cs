using Cinemachine;
using GenshinImpactMovementSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCameraController : MonoBehaviour {
    
    //[field: SerializeField] public bool UsingOrbitaCamera { get; private set; }
    [field: SerializeField] public GameObject[] playerParts { get; private set; }

    [field: SerializeField] public PlayerInput input { get; private set; }

    CinemachineVirtualCamera activeCamera { get; set; }
    int activeCameraPriorityModifier { get; set; } = 31337;

    [field: SerializeField] public Camera MainCamera { get; private set; }
    private CinemachineBrain cinemachineBrain { get; set; }
    [field: SerializeField] public CinemachineVirtualCamera cinemachine1stPerson { get; set; }
    [field: SerializeField] public CinemachineVirtualCamera cinemachine3ndPerson { get; set; }
    //[field: SerializeField] public CinemachineVirtualCamera cinemachineOrvit { get; set; }
    
    [Header("1stPersonVariables")]
    Vector3 playerMoveInput = Vector3.zero;
    Vector3 playerLookInput = Vector3.zero;
    Vector3 previousPlayerLookInput = Vector3.zero;
    [field: SerializeField] public bool InvertedMouseY1stP { get; private set; } = true;
    [field: SerializeField] public float playerLookInputLerpTime { get; private set; } = 0.35f;
#pragma warning disable CS0108 // El miembro oculta el miembro heredado. Falta una contraseņa nueva
    [field: SerializeField] public Rigidbody rigidbody { get; private set; }
#pragma warning restore CS0108 // El miembro oculta el miembro heredado. Falta una contraseņa nueva
    [field: SerializeField] public float rotationSpeedMultiplier { get; private set; } = 90f;

    private void Awake() {
        cinemachineBrain = MainCamera.GetComponent<CinemachineBrain>();
    }

    // Start is called before the first frame update
    void Start() {
        ChangeCamera(); // First time through, lets set the camera.
    }

    // Update is called once per frame
    void Update() {
        if (input.ChangeCameraWasPressedThisFrame)
            ChangeCamera();
    }

    private void FixedUpdate() {
        if (cinemachine1stPerson == activeCamera) {
            playerLookInput = GetLookInput();
            PlayerLook();
        }
    }

    private void PlayerLook() {
        rigidbody.rotation = Quaternion.Euler(0f, rigidbody.rotation.eulerAngles.y + (playerLookInput.x * rotationSpeedMultiplier), 0f);
    }

    private Vector3 GetLookInput() {
        previousPlayerLookInput = playerLookInput;
        playerMoveInput = new Vector3(input.LookInput.x, (InvertedMouseY1stP ? -input.LookInput.y : input.LookInput.y), 0f);
        return Vector3.Lerp(previousPlayerLookInput, playerMoveInput * Time.fixedDeltaTime, playerLookInputLerpTime);
    }

    void ChangeCamera() {
        if (cinemachine3ndPerson == activeCamera) {
            SetCameraPriorities(cinemachine3ndPerson, cinemachine1stPerson);
            //UsingOrbitaCamera = false;
            //MainCamera.cullingMask &= ~(1 << LayerMask.NameToLayer("Player(Self)"));
            StartCoroutine(DisablePlayerParts(cinemachineBrain.m_DefaultBlend.BlendTime));
        }
        else if (cinemachine1stPerson == activeCamera) {
            SetCameraPriorities(cinemachine1stPerson, cinemachine3ndPerson);
            activeCamera = cinemachine3ndPerson;
            //UsingOrbitaCamera = false;
        }
        else {  // for the first time through or if there's an error.
            cinemachine3ndPerson.Priority += activeCameraPriorityModifier;
            activeCamera = cinemachine3ndPerson;
        }
    }

    private void SetCameraPriorities(CinemachineVirtualCamera currentCameraMode, CinemachineVirtualCamera newCameraMode) {
        currentCameraMode.Priority -= activeCameraPriorityModifier;
        newCameraMode.Priority += activeCameraPriorityModifier;
        activeCamera = newCameraMode;

        /*if (newCameraMode != cinemachine1stPerson) {
            if (playerParts != null && playerParts.Length > 0) {
                if (!playerParts[0].activeInHierarchy) {
                    foreach (GameObject part in playerParts) {
                        part.SetActive(true);
                    }
                }
            }
        }*/
    }

    IEnumerator DisablePlayerParts(float blendTime) {
        yield return new WaitForSeconds(blendTime);
        
        /*foreach (GameObject part in playerParts) {
            part.SetActive(false);
        }*/
    }
}
