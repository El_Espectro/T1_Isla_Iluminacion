using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Hellmade.Sound;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleSoundSystem : MonoBehaviour {
    
    private ParticleSystem _parentParticleSystem;

    private int _currentNumberOfParticles = 0;

    [field: SerializeField] private AudioClip[] BornSounds;
    [field: SerializeField] private AudioClip[] DieSounds;

    private Transform _playerTransform;

    private AudioSource _audioSource;

    private void Awake() {
        _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Start is called before the first frame update
    void Start() {
        _parentParticleSystem = GetComponent<ParticleSystem>();
        if (_parentParticleSystem == null)
            Debug.LogError("Missing ParticleSystem!", this);

        if (_audioSource == null)
            _audioSource = GetComponent<AudioSource>();

        EazySoundManager.IgnoreDuplicateSounds = false;
    }

    // Update is called once per frame
    void Update() {
        var amount = Mathf.Abs(_currentNumberOfParticles - _parentParticleSystem.particleCount);

        if (_parentParticleSystem.particleCount < _currentNumberOfParticles) {
            StartCoroutine(PlaySound(DieSounds[Random.Range(0, DieSounds.Length)], amount));
        }

        if (_parentParticleSystem.particleCount > _currentNumberOfParticles) {
            StartCoroutine(PlaySound(BornSounds[Random.Range(0, BornSounds.Length)], amount));
        }

        _currentNumberOfParticles = _parentParticleSystem.particleCount;
    }

    private IEnumerator PlaySound(AudioClip clip, int amount) {
        var distanceToPlayer = Vector3.Distance(this.transform.position, _playerTransform.position);
        var soundDelay = distanceToPlayer / 343; //Speed of sound https://en.wikipedia.org/wiki/Speed_of_sound

        //Debug.Log($"Distance to player '{distanceToPlayer}', therefore delayed sound of '{soundDelay}' sec.");

        for (int i = 0; i < amount; i++) {
            //Debug.Log($"Play sound: '{clip.name}'");
            int soundId = EazySoundManager.PrepareSound(clip, Random.Range(0.8f, 1.2f), false, this.transform);
            Audio sound = EazySoundManager.GetSoundAudio(soundId);

            sound.SetVolume(1.0f);
            sound.Min3DDistance = 20;
            sound.Max3DDistance = 175; //250;
            sound.SpatialBlend = 1f;
            sound.Spread = 60f;
            sound.DopplerLevel = 0f;
            sound.Pitch = Random.Range(0.8f, 1.2f);
            sound.RolloffMode = AudioRolloffMode.Custom;

            AnimationCurve animCurve = new AnimationCurve(
                                                new Keyframe(sound.Min3DDistance, 1f),
                                                new Keyframe(sound.Min3DDistance + (sound.Max3DDistance - sound.Min3DDistance) / 4f, .35f),
                                                new Keyframe(sound.Max3DDistance, 0f));
            animCurve.SmoothTangents(1, .025f);

            StartCoroutine(PlaySound(sound, soundDelay, animCurve));

            //Attempt to avoid multiple of the same audio being played at the exact same time - as it sounds wierd
            yield return new WaitForSeconds(0.05f);
        }
    }

    private IEnumerator PlaySound(Audio sound, float delayInSeconds, AnimationCurve animCurve) {
        _audioSource.SetCustomCurve(AudioSourceCurveType.CustomRolloff, animCurve);
        yield return new WaitForSeconds(delayInSeconds);

        //_audioSource.PlayOneShot(sound.Clip, sound.Volume);
        /*_audioSource.clip = sound.Clip;
        _audioSource.volume = sound.Volume;
        _audioSource.Play();*/
        sound.Play();
    }
}