using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GenericManager : MonoBehaviour {

    public static GenericManager instance { get; private set; }
    [field: SerializeField] public HumanoidLandInput input { get; private set; }

    public bool onPause { get; private set; }
    [field: SerializeField] public GameObject pauseMenu { get; private set; }

    // Start is called before the first frame update
    void Start() {
        CursorOnOff();
        pauseMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update() {
        if (input.PauseWasPressedThisFrame)
            Pause();

    }

    public void Pause() {
        if (!onPause) {
            onPause = true;
            pauseMenu.SetActive(true);
            Time.timeScale = 0;
            CursorOnOff();
        }
        else {
            onPause = false;
            pauseMenu.SetActive(false);
            Time.timeScale = 1;
            CursorOnOff();
        }
    }

    public void RestartLevel() {
        ResetBeforeChangeLevel();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void CursorOnOff() {
        if (Cursor.lockState == CursorLockMode.Locked) {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    private void ResetBeforeChangeLevel() {
        onPause = false;
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
        /*Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;*/
    }

    public void QuitGame() {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
