using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuideRocket : MonoBehaviour {

    [field: SerializeField] private Rigidbody rg { get; set; }
    [field: SerializeField] private ConstantForce cf { get; set; }
    [field: SerializeField] private Collider coll { get; set; }
    [field: SerializeField] private Animator anim { get; set; }
    [field: SerializeField] private float ignitionTime { get; set; } = 1f;
    [field: SerializeField] private float flightTime { get; set; } = 3f;

    [field: SerializeField] private KeyCode interactionKey { get; set; } = KeyCode.F;
    [field: SerializeField] private Image interactionKeyHud { get; set; }
    private bool isInteractable { get; set; } = false;
    private bool isIgnited { get; set; } = false;

    [field: SerializeField] private AudioSource ignitionSound { get; set; }
    [field: SerializeField] private AudioSource launchSound { get; set; }
    [field: SerializeField] private AudioClip launchSoundv2 { get; set; }
    private AudioSource audioSource { get; set; }

    private void Start() {
        if (rg == null)
            rg = GetComponent<Rigidbody>();
        if (cf == null)
            cf = GetComponent<ConstantForce>();
        if (coll == null)
            coll = GetComponent<Collider>();
        if (anim == null)
            anim = GetComponent<Animator>();
        if (audioSource == null)
            audioSource = GetComponent<AudioSource>();

        cf.enabled = false;
        interactionKeyHud.gameObject.SetActive(false);
    }

    private void Update() {
        if (!isInteractable || isIgnited)
            return;
        else {
            if (isInteractable && Input.GetKeyDown(interactionKey)) {
                StartCoroutine(StartRocketCo());
            }
        }
        
    }

    IEnumerator StartRocketCo() {
        isIgnited = true;
        interactionKeyHud.gameObject.SetActive(false);
        isInteractable = false;
        anim.SetTrigger("Ignition");
        ignitionSound.Play();
        yield return new WaitForSeconds(ignitionTime);
        ignitionSound.Stop();
        
        cf.enabled = true;
        audioSource.PlayOneShot(launchSound.clip);
        //AudioSource.PlayClipAtPoint(launchSound.clip, transform.position);
        yield return new WaitForSeconds(flightTime);
        rg.isKinematic = true;
        cf.enabled = false;
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player") && !isIgnited) {
            interactionKeyHud.gameObject.SetActive(true);
            isInteractable = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Player") && !isIgnited) {
            interactionKeyHud.gameObject.SetActive(false);
            isInteractable = false;
        }
    }
}
