using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Detonator : MonoBehaviour {

    [field: SerializeField] private GameObject[] fireworks { get; set; }
    [field: SerializeField] private float delay { get; set; } = 3f;
    [field: SerializeField] private SphereCollider coll { get; set; }

    [field: SerializeField] private KeyCode interactionKey { get; set; } = KeyCode.F;
    [field: SerializeField] private Image interactionKeyHud { get; set; }
    private bool isInteractable { get; set; }
    private bool isDetonated { get; set; }

    [field: SerializeField] private AudioSource musicSource { get; set; }

    IEnumerator Start() {
        if (coll == null)
            coll = GetComponent<SphereCollider>();

        yield return null;
        /*yield return new WaitForSeconds(3f);
        fireworks = GameObject.FindGameObjectsWithTag("PS_Firework");

        foreach (GameObject firework in fireworks) {
            ParticleSystem aux = firework.GetComponent<ParticleSystem>();
            aux.Stop(true);
        }*/

        if (fireworks[0].activeInHierarchy) {
            foreach (GameObject firework in fireworks) {
                firework.SetActive(false);
            }
        }
        
    }
    private void Update() {
        if (!isInteractable || isDetonated)
            return;
        else {
            if (isInteractable && Input.GetKeyDown(interactionKey)) {
                StartCoroutine(StartFireworkDisplayCo());
                musicSource.volume = 0.5f;
            }
        }

    }

    IEnumerator StartFireworkDisplayCo() {

        isDetonated = true;
        interactionKeyHud.gameObject.SetActive(false);
        isInteractable = false;
        
        yield return new WaitForSeconds(delay);

        foreach (GameObject firework in fireworks) {
            firework.SetActive(true);
            yield return new WaitForSeconds(0.5f);
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player") && !isDetonated) {
            interactionKeyHud.gameObject.SetActive(true);
            isInteractable = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Player") && !isDetonated) {
            interactionKeyHud.gameObject.SetActive(false);
            isInteractable = false;
        }
    }
}
