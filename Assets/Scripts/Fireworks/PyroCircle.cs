using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PyroCircle : MonoBehaviour {

    [field: SerializeField] private Animator anim { get; set; }
    [field: SerializeField] private ParticleSystem fog { get; set; }
    //[field: SerializeField] private float fogFadingTime { get; set; } = 1f;
    private bool isIgnited { get; set; }

    [field: SerializeField] private AudioSource ignitedSound { get; set; }
    private AudioSource audioSource { get; set; }

    private void Start() {
        if (anim == null)
            anim = GetComponent<Animator>();
        if (audioSource == null)
            audioSource = GetComponent<AudioSource>();

        audioSource.clip = ignitedSound.clip;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Rocket") && !isIgnited) {
            isIgnited = true;
            anim.SetBool("Ignited", true);
            StartCoroutine(DisableFogCo());
        }
    }

    IEnumerator DisableFogCo() {
        //audioSource.PlayOneShot(ignitedSound.clip);
        audioSource.Play();

        // Stop emission but don't clear particles
        fog.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        yield return new WaitForSeconds(fog.main.startLifetime.constant);
        fog.gameObject.SetActive(false);
    }
}
